import React, { useState } from 'react'
import './App.css'
import { BannerContainer } from './components/BannerContainer'
import iconbutton from './images/iconbutton.png'
import menu from './images/menu.png'

function App() {
  const [openedWishlist, setOpenedWishlist] = useState(false)

  const [wishlist, setWishlist] = useState([])

  const images = [
    'Baton',
    'Chokito',
    'Galak',
    'Prestígio',
    'Serenata',
    'Suflair'
  ]

  function addInWishlist(image) {
    const myActualWishlist = [...wishlist]
    if (myActualWishlist.indexOf(image) !== -1) {
      return
    }
    myActualWishlist.push(image)
    setWishlist(myActualWishlist)
  }

  function removeInWishlist(image) {
    const myActualWishlist = [...wishlist]
    myActualWishlist.splice(myActualWishlist.indexOf(image), 1)
    setWishlist(myActualWishlist)
  }

  return (
    <div className="main-container">
      <div className="box-container">
        <h1>Wishlist</h1>
        <button
          className="button-wishlist"
          onClick={() => setOpenedWishlist(!openedWishlist)}
        >
          <img src={iconbutton} alt="" className="image-button" />
          Minha Wishlist
        </button>

        <div className="seccion-container">
          {images.map(image => (
            <BannerContainer
              key={image}
              image={image}
              namebutton={'Adicionar'}
              addInWishlist={addInWishlist}
            />
          ))}
        </div>
      </div>
      <div
        className={
          openedWishlist ? 'wishlist-container-modal' : 'wishlist-closed'
        }
      >
        <div className="wishlist-container">
          {wishlist.map(image => (
            <BannerContainer
              key={image}
              image={image}
              namebutton="Remover"
              removeInWishlist={removeInWishlist}
            />
          ))}
        </div>
      </div>
    </div>
  )
}

export default App
