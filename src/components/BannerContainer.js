import React from 'react'

export function BannerContainer({ image, namebutton, addInWishlist, removeInWishlist }) {
  return (
    <div className="banner-container">
      <h2>{image}</h2>

      <img
        src={`http://localhost:3000/images/${image}.jpg`}
        alt={image}
        width={'100%'}
        className="images-choco"
      />

      <button className="button-container" onClick={() => namebutton === 'Adicionar' ? addInWishlist(image) : removeInWishlist(image)}>{namebutton}</button>
    </div>
  )
}
